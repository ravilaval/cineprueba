package com.example.cineprueba.helpers

import android.content.Context
import android.content.SharedPreferences

class PreferencesHelper(context: Context?) {
    companion object{
        val TOKEN_TYPE = "token_type"
        val ACCESS_TOKEN = "access_token"
        val USER = "user"
        val MOVIES = "movies"
        val MOVIE = "movie"
        val TRAILER_ROUTE = "trailer_route"
        val CINEMA = "cinema"
        val NAME = "Preferences_Login"
    }

    private val preferences: SharedPreferences by lazy {
        context!!.getSharedPreferences(NAME, Context.MODE_PRIVATE)
    }

    fun saveString(key: String, data: String){
        this.preferences.edit().putString(key, data).apply()
    }

    fun getString(key: String, defValue: String): String{
        return this.preferences.getString(key, defValue) ?: defValue
    }

    fun deleteString(key: String){
        this.preferences.edit().remove(key).apply()
    }
}