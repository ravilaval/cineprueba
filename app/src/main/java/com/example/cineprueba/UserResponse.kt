package com.example.cineprueba

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

data class UserResponse(
    @SerializedName("email") private val _email: String?,
    @SerializedName("first_name") private val _firstName: String?,
    @SerializedName("last_name") private val _lastName: String?,
    @SerializedName("phone_number") private val _phoneNumber: String?,
    @SerializedName("profile_picture") private val _profilePicture: String?,
    @SerializedName("card_number") private val _cardNumber: String?): Parcelable {

    val email: String
        get() = this._email ?: ""

    val firstName: String
        get() = this._firstName ?: ""

    val lastName: String
        get() = this._lastName ?: ""

    val phoneNumber: String
        get() = this._phoneNumber ?: ""

    val profilePicture: String
        get() = this._profilePicture ?: ""

    val cardNumber: String
        get() = this._cardNumber ?: ""

    constructor(parcel: Parcel) : this(
        parcel.readString() ?: "",
        parcel.readString() ?: "",
        parcel.readString() ?: "",
        parcel.readString() ?: "",
        parcel.readString() ?: "",
        parcel.readString() ?: ""
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(_email)
        parcel.writeString(_firstName)
        parcel.writeString(_lastName)
        parcel.writeString(_phoneNumber)
        parcel.writeString(_profilePicture)
        parcel.writeString(_cardNumber)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<UserResponse> {
        override fun createFromParcel(parcel: Parcel): UserResponse {
            return UserResponse(parcel)
        }

        override fun newArray(size: Int): Array<UserResponse?> {
            return arrayOfNulls(size)
        }
    }

}