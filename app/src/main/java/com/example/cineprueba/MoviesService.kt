package com.example.cineprueba

import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.Query

interface MoviesService {
    @GET("v2/movies")
    suspend fun getMovies(@Header("api_key") apiKey: String,
                  @Query("country_code") contryCode: String,
                  @Query("cinemas") cinemas: String): Response<MoviesResponse>
}