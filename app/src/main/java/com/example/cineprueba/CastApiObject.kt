package com.example.cineprueba

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

data class CastApiObject(
    @SerializedName("label") private val _label: String?,
    @SerializedName("value") private val _value: List<String>?):Parcelable {

    val label: String
        get() = this._label ?: ""

    val value: List<String>
        get() = this._value ?: emptyList()

    constructor(parcel: Parcel) : this(
        parcel.readString() ?: "",
        parcel.createStringArrayList() ?: emptyList()
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(_label)
        parcel.writeStringList(_value)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<CastApiObject> {
        override fun createFromParcel(parcel: Parcel): CastApiObject {
            return CastApiObject(parcel)
        }

        override fun newArray(size: Int): Array<CastApiObject?> {
            return arrayOfNulls(size)
        }
    }
}
