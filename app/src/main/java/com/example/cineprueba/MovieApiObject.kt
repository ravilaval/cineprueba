package com.example.cineprueba

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

data class MovieApiObject(@SerializedName("rating") private val _rating: String?,
                          @SerializedName("media") private val _media: List<MediaApiObject>?,
                          @SerializedName("cast") private val _cast: List<CastApiObject>?,
                          @SerializedName("cinemas") private val _cinemas: List<Int>?,
                          @SerializedName("position") private val _position: Int?,
                          @SerializedName("categories") private val _categories: List<String>?,
                          @SerializedName("genre") private val _genre: String?,
                          @SerializedName("synopsis") private val _synopsis: String?,
                          @SerializedName("length") private val _length: String?,
                          @SerializedName("release_date") private val _releaseDate: String?,
                          @SerializedName("distributor") private val _distributor: String?,
                          @SerializedName("id") private val _id: Int?,
                          @SerializedName("name") private val _name: String?,
                          @SerializedName("code") private val _code: String?,
                          @SerializedName("original_name") private val _originalName: String?):Parcelable {

    val rating: String
        get() = this._rating ?: ""

    val media: List<MediaApiObject>
        get() = this._media ?: emptyList()

    val cast: List<CastApiObject>
        get() = this._cast ?: emptyList()

    val cinemas: List<Int>
        get() = this._cinemas ?: emptyList()

    val position: Int
        get() = this._position ?: -1

    val categories: List<String>
        get() = this._categories ?: emptyList()

    val genre: String
        get() = this._genre ?: ""

    val synopsis: String
        get() = this._synopsis ?: ""

    val length: String
        get() = this._length ?: ""

    val releaseDate: String
        get() = this._releaseDate ?: ""

    val distributor: String
        get() = this._distributor ?: ""

    val id: Int
        get() = this._id ?: -1

    val name: String
        get() = this._name ?: ""

    val code: String
        get() = this._code ?: ""

    val originalName: String
        get() = this._originalName ?: ""

    constructor(parcel: Parcel) : this(
        parcel.readString() ?: "",
        parcel.createTypedArrayList(MediaApiObject) ?: emptyList(),
        parcel.createTypedArrayList(CastApiObject) ?: emptyList(),
        parcel.createIntArray()?.toList() ?: emptyList(),
        parcel.readInt(),
        parcel.createStringArrayList() ?: emptyList(),
        parcel.readString() ?: "",
        parcel.readString() ?: "",
        parcel.readString() ?: "",
        parcel.readString() ?: "",
        parcel.readString() ?: "",
        parcel.readInt(),
        parcel.readString() ?: "",
        parcel.readString() ?: "",
        parcel.readString() ?: ""
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(_rating)
        parcel.writeTypedList(_media)
        parcel.writeTypedList(_cast)
        parcel.writeValue(_position)
        parcel.writeStringList(_categories)
        parcel.writeString(_genre)
        parcel.writeString(_synopsis)
        parcel.writeString(_length)
        parcel.writeString(_releaseDate)
        parcel.writeString(_distributor)
        parcel.writeValue(_id)
        parcel.writeString(_name)
        parcel.writeString(_code)
        parcel.writeString(_originalName)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<MovieApiObject> {
        override fun createFromParcel(parcel: Parcel): MovieApiObject {
            return MovieApiObject(parcel)
        }

        override fun newArray(size: Int): Array<MovieApiObject?> {
            return arrayOfNulls(size)
        }
    }

}
