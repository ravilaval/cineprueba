package com.example.cineprueba.view

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import com.example.cineprueba.R
import com.example.cineprueba.TokenService
import com.example.cineprueba.databinding.ActivityLoginBinding
import com.example.cineprueba.helpers.PreferencesHelper
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

class LoginActivity : AppCompatActivity() {

    private lateinit var binding: ActivityLoginBinding

    private val preferencesHelper: PreferencesHelper by lazy {
        PreferencesHelper(this)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityLoginBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.etEmail.setText("pruebas_beto_ia@yahoo.com")
        binding.etPassword.setText("Pruebas01")

        binding.btnLogin.setOnClickListener {
            binding.progressContainer.visibility = View.VISIBLE
            getToken(binding.etEmail.text.toString().trim(), binding.etPassword.text.toString().trim())
        }
    }

    private fun getRetrofit(): Retrofit {
        val client = OkHttpClient()
            .newBuilder()
            .connectTimeout(30, TimeUnit.SECONDS)
            .readTimeout(30, TimeUnit.SECONDS)
            .writeTimeout(30, TimeUnit.SECONDS)
            .build()

        return Retrofit.Builder()
            .baseUrl("https://stage-api.cinepolis.com/")
            .addConverterFactory(GsonConverterFactory.create())
            .client(client)
            .build()
    }

    private fun getToken(user:String, password:String){
        CoroutineScope(Dispatchers.IO).launch {
            val call = getRetrofit().create(TokenService::class.java).getUserToken(
                "stage_HNYh3RaK_Test",
                "MX",
                user,
                password,
                "password", "IATestCandidate",
                "c840457e777b4fee9b510fbcd4985b68"
            )
            val token = call.body()
            runOnUiThread{
                binding.progressContainer.visibility = View.GONE
                if(call.isSuccessful){

                    if(token != null){
                        preferencesHelper.saveString(PreferencesHelper.TOKEN_TYPE, token.tokenType)
                        preferencesHelper.saveString(PreferencesHelper.ACCESS_TOKEN, token.accessToken)
                    }

                    val intent = Intent(applicationContext, MainActivity::class.java)
                    intent.putExtra(PreferencesHelper.TOKEN_TYPE, token?.tokenType)
                    intent.putExtra(PreferencesHelper.ACCESS_TOKEN, token?.accessToken)
                    startActivity(intent)

                    finish()
                }else{
                    showError()
                }

            }
        }
    }

    private fun showError() {
        Toast.makeText(this, R.string.error, Toast.LENGTH_SHORT).show()
    }
}