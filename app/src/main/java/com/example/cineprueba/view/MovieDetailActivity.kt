package com.example.cineprueba.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.MediaController
import com.example.cineprueba.MovieApiObject
import com.example.cineprueba.R
import com.example.cineprueba.SimpleMovie
import com.example.cineprueba.databinding.ActivityMovieDetailBinding
import com.example.cineprueba.helpers.PreferencesHelper

class MovieDetailActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMovieDetailBinding

    private val movie: SimpleMovie?
        get() = this.intent.getParcelableExtra(PreferencesHelper.MOVIE)

    private val trailerRoute: String?
        get() = this.intent.getStringExtra(PreferencesHelper.TRAILER_ROUTE)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMovieDetailBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val urlTrailer = this.trailerRoute+this.movie!!.media
        binding.vvMovieTrailer.setVideoPath(urlTrailer)
        val mediaController = MediaController(this)
        binding.vvMovieTrailer.setMediaController(mediaController)
        mediaController.setAnchorView(binding.vvMovieTrailer)
        binding.vvMovieTrailer.start()

        binding.tvName.text = this.movie?.name
        binding.tvRating.text = this.movie?.rating
        binding.tvGenre.text = this.movie?.genre
        binding.tvLength.text = this.movie?.length
        binding.tvSynopsis.text = this.movie?.synopsis

        binding.ibtnBack.setOnClickListener{
            finish()
        }
    }
}