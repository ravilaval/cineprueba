package com.example.cineprueba.view

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.cineprueba.MovieApiObject
import com.example.cineprueba.R
import com.example.cineprueba.databinding.ItemMovieBinding

class MoviesAdapter(private val context: Context?,
                    private val posterRoute: String,
                    private val trailerRoute: String,
                    private val callback: Callback
                    ) : RecyclerView.Adapter<MoviesAdapter.ViewHolder>(), View.OnClickListener {

    interface Callback {
        fun onMovieSelected(movie: MovieApiObject, trailerRoute: String)
    }

    private val movieList: MutableList<MovieApiObject> = mutableListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        return ViewHolder(layoutInflater.inflate(R.layout.item_movie, parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val movie = this.movieList[position]
        holder.bind(this.context!!, movie, this.posterRoute, this)
    }

    override fun getItemCount(): Int {
        return this.movieList.size
    }

    fun updateMovies(movies: List<MovieApiObject>){
        this.movieList.clear()
        this.movieList.addAll(movies)
        this.notifyDataSetChanged()
    }

    override fun onClick(v: View?) {
        val c = v?.tag as? MovieApiObject
        if(c != null){
            this.callback.onMovieSelected(c, this.trailerRoute)
        }
    }

    class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        private val binding = ItemMovieBinding.bind(itemView)

        fun bind(context: Context, movie: MovieApiObject, posterRoute: String, clickListener: View.OnClickListener){
            binding.cvMoviePosterContainer.tag = movie
            binding.cvMoviePosterContainer.setOnClickListener(clickListener)

            val urlPoster = posterRoute+movie.media[0].resource
            Glide.with(context)
                .load(urlPoster)
                .placeholder(R.drawable.no_disponible)
                .into(binding.ivMoviePoster)

            binding.tvMovieTitle.text = movie.name
        }

    }

}
