package com.example.cineprueba.view

import android.app.AlertDialog
import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.example.cineprueba.R
import com.example.cineprueba.UserResponse
import com.example.cineprueba.databinding.FragmentPerfilBinding
import com.example.cineprueba.helpers.PreferencesHelper

class PerfilFragment : Fragment() {

    companion object{
        fun newInstance(user: UserResponse): PerfilFragment{
            val fragment = PerfilFragment()
            val args = Bundle()
            args.putParcelable(PreferencesHelper.USER, user)
            fragment.arguments = args
            return fragment
        }
    }

    private val preferencesHelper: PreferencesHelper by lazy {
        PreferencesHelper(activity)
    }

    private var _binding: FragmentPerfilBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentPerfilBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val args = arguments
        if(args != null){
            val user = args.getParcelable<UserResponse>(PreferencesHelper.USER)
            val fullName = "${user?.firstName} ${user?.lastName}"

            binding.tvName.text = fullName
            binding.tvEmail.text = user?.email
        }

        val dialog = createCloseDialog()
        binding.btnLogout.setOnClickListener { dialog.show() }
    }

    private fun createCloseDialog():AlertDialog{
        val builder = AlertDialog.Builder(this.context)
        builder.setMessage(R.string.log_out_message)
        builder.setTitle(R.string.log_out)
        builder.setPositiveButton(R.string.ok, DialogInterface.OnClickListener(
            fun (dialog: DialogInterface, id: Int){
                this.logOut()
            })
        )
        builder.setNegativeButton(R.string.cancel, DialogInterface.OnClickListener(
            fun(dialog: DialogInterface, id: Int){

            })
        )

        return builder.create()
    }

    private fun logOut() {
        preferencesHelper.deleteString(PreferencesHelper.TOKEN_TYPE)
        preferencesHelper.deleteString(PreferencesHelper.ACCESS_TOKEN)
        startActivity(Intent(activity, LoginActivity::class.java))
        Toast.makeText(activity,R.string.log_out_confirm, Toast.LENGTH_SHORT).show()
        activity?.onBackPressed()
    }

}