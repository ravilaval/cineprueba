package com.example.cineprueba.view

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.cineprueba.MovieApiObject
import com.example.cineprueba.MoviesResponse
import com.example.cineprueba.SimpleMovie
import com.example.cineprueba.databinding.FragmentCarteleraBinding
import com.example.cineprueba.helpers.PreferencesHelper

class CarteleraFragment : Fragment(), MoviesAdapter.Callback {

    companion object{
        fun newInstance(movies: MoviesResponse): CarteleraFragment{
            val fragment = CarteleraFragment()
            val args = Bundle()
            args.putParcelable(PreferencesHelper.MOVIES, movies)
            fragment.arguments = args
            return fragment
        }
    }

    private var _binding: FragmentCarteleraBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentCarteleraBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val args = arguments
        if(args != null){
            val moviesResponse = args.getParcelable<MoviesResponse>(PreferencesHelper.MOVIES)

            val moviesAdapter = MoviesAdapter(this.context, moviesResponse!!.routes[0].sizes!!.large,
                moviesResponse!!.routes[2].sizes!!.medium, this)
            moviesAdapter.updateMovies(moviesResponse.movies)

            binding.rvMovies.adapter = moviesAdapter
            binding.rvMovies.layoutManager = GridLayoutManager(activity,3,
                RecyclerView.VERTICAL,false)
            binding.rvMovies.setHasFixedSize(true)
        }
    }

    override fun onMovieSelected(movie: MovieApiObject, trailerRoute: String) {
        Toast.makeText(this.context, movie.name, Toast.LENGTH_SHORT).show()

        val movieSimple = SimpleMovie(movie.name, movie.rating, movie.genre, movie.length, movie.synopsis, movie.media[2].resource)

        val intent = Intent(this.context, MovieDetailActivity::class.java)
        intent.putExtra(PreferencesHelper.MOVIE, movieSimple)
        intent.putExtra(PreferencesHelper.TRAILER_ROUTE,trailerRoute)
        startActivity(intent)
    }

}