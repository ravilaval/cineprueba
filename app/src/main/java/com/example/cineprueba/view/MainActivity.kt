package com.example.cineprueba.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import com.example.cineprueba.*
import com.example.cineprueba.databinding.ActivityMainBinding
import com.example.cineprueba.helpers.PreferencesHelper
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

class MainActivity : AppCompatActivity() {

    private lateinit var binding:ActivityMainBinding

    private lateinit var mActiveFragment:Fragment
    private lateinit var mFragmentManager:FragmentManager

    private val tokenType: String?
        get() = this.intent.getStringExtra(PreferencesHelper.TOKEN_TYPE)

    private val accesToken: String?
        get() = this.intent.getStringExtra(PreferencesHelper.ACCESS_TOKEN)

    private lateinit var mUser:UserResponse
    private lateinit var mMovies:MoviesResponse

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        getUser(tokenType!!,accesToken!!)
        //setupBottomNav()
    }

    private fun setupBottomNav(){
        mFragmentManager = supportFragmentManager

        val perfilFragment = PerfilFragment.newInstance(mUser)
        val carteleraFragment = CarteleraFragment.newInstance(mMovies)

        mActiveFragment = perfilFragment

        //Al ocultar y mostrar los Fragmentos los que se haga en estos no se perdera
        mFragmentManager.beginTransaction()
            .add(R.id.hostFragment, carteleraFragment, CarteleraFragment::class.java.name)
            .hide(carteleraFragment)
            .commit()
        mFragmentManager.beginTransaction()
            .add(R.id.hostFragment, perfilFragment, PerfilFragment::class.java.name)
            .commit()

        binding.bottomNav.setOnItemSelectedListener { item ->
            when(item.itemId){
                R.id.action_perfil -> {
                    mFragmentManager.beginTransaction().hide(mActiveFragment).show(perfilFragment).commit()
                    mActiveFragment = perfilFragment
                    true
                }
                R.id.action_cartelera -> {
                    mFragmentManager.beginTransaction().hide(mActiveFragment).show(carteleraFragment).commit()
                    mActiveFragment = carteleraFragment
                    true
                }
                else -> false
            }
        }
    }

    private fun getRetrofit(): Retrofit {
        val client = OkHttpClient()
            .newBuilder()
            .connectTimeout(300, TimeUnit.SECONDS)
            .readTimeout(300, TimeUnit.SECONDS)
            .writeTimeout(300, TimeUnit.SECONDS)
            .build()

        return Retrofit.Builder()
            .baseUrl("https://stage-api.cinepolis.com/")
            .addConverterFactory(GsonConverterFactory.create())
            .client(client)
            .build()
    }

    private fun getUser(tokenType:String, accesToken:String){
        binding.progressContainer.visibility = View.VISIBLE

        CoroutineScope(Dispatchers.IO).launch {
            val call = getRetrofit().create(UserService::class.java).getUser(
                "stage_HNYh3RaK_Test",
                "$tokenType $accesToken",
                "MX")
            val token = call.body()
            runOnUiThread{
                binding.progressContainer.visibility = View.GONE
                if(call.isSuccessful){
                    mUser = token!!
                    getMovies()
                }else{
                    showError()
                }

            }
        }
    }

    private fun getMovies(){
        binding.progressContainer.visibility = View.VISIBLE

        CoroutineScope(Dispatchers.IO).launch {
            val call = getRetrofit().create(MoviesService::class.java).getMovies(
                "stage_HNYh3RaK_Test",
                "MX",
                "32")
            val token = call.body()
            runOnUiThread{
                binding.progressContainer.visibility = View.GONE
                if(call.isSuccessful){
                    mMovies = token!!
                    setupBottomNav()
                }else{
                    showError()
                }

            }
        }
    }

    private fun showError() {
        Toast.makeText(this, R.string.error, Toast.LENGTH_SHORT).show()
    }

}