package com.example.cineprueba

import android.os.Parcel
import android.os.Parcelable

data class SimpleMovie(val name:String, val rating:String, val genre:String, val length:String, val synopsis:String, val media:String):Parcelable{
    constructor(parcel: Parcel) : this(
        parcel.readString() ?: "",
        parcel.readString() ?: "",
        parcel.readString() ?: "",
        parcel.readString() ?: "",
        parcel.readString() ?: "",
        parcel.readString() ?: ""
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(name)
        parcel.writeString(rating)
        parcel.writeString(genre)
        parcel.writeString(length)
        parcel.writeString(synopsis)
        parcel.writeString(media)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<SimpleMovie> {
        override fun createFromParcel(parcel: Parcel): SimpleMovie {
            return SimpleMovie(parcel)
        }

        override fun newArray(size: Int): Array<SimpleMovie?> {
            return arrayOfNulls(size)
        }
    }
}