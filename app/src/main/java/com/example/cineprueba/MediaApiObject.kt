package com.example.cineprueba

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

data class MediaApiObject(@SerializedName("resource") private val _resource: String?,
                          @SerializedName("type") private val _type: String?,
                          @SerializedName("code") private val _code:String?):Parcelable {

    val resource: String
        get() = this._resource ?: ""

    val type: String
        get() = this._type ?: ""

    val code: String
        get() = this._code ?: ""

    constructor(parcel: Parcel) : this(
        parcel.readString() ?: "",
        parcel.readString() ?: "",
        parcel.readString() ?: ""
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(_resource)
        parcel.writeString(_type)
        parcel.writeString(_code)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<MediaApiObject> {
        override fun createFromParcel(parcel: Parcel): MediaApiObject {
            return MediaApiObject(parcel)
        }

        override fun newArray(size: Int): Array<MediaApiObject?> {
            return arrayOfNulls(size)
        }
    }

}
