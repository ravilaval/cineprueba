package com.example.cineprueba

import com.google.gson.annotations.SerializedName

data class TokenResponse(@SerializedName("access_token") private val _accessToken: String?,
                         @SerializedName("token_type") private val _tokenType: String?,
                         @SerializedName("expires_in") private val _expiresIn: Long?,
                         @SerializedName("refresh_token") private val _refreshToken: String?,
                         @SerializedName("as:client_id") private val _clientId: String?,
                         @SerializedName("username") private val _userName: String?,
                         @SerializedName("country_code") private val _countryCode: String?,
                         @SerializedName(".issued") private val _issued: String?,
                         @SerializedName(".expires") private val _expires: String?) {

    val accessToken: String
        get() = this._accessToken ?: ""

    val tokenType: String
        get() = this._tokenType ?: ""

    val expiresIn: Long
        get() = this._expiresIn ?: -1

    val refreshToken: String
        get() = this._refreshToken ?: ""

    val clientId: String
        get() = this._clientId ?: ""

    val userName: String
        get() = this._userName ?: ""

    val countryCode: String
        get() = this._countryCode ?: ""

    val issued: String
        get() = this._issued ?: ""

    val expires: String
        get() = this._expires ?: ""
}