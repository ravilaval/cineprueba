package com.example.cineprueba

import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.Query

interface UserService {
    @GET("v1/members/profile")
    suspend fun getUser(@Header("api_key") apiKey: String,
                @Header("Authorization") authorization: String,
                @Query("country_code") countryCode: String): Response<UserResponse>
}