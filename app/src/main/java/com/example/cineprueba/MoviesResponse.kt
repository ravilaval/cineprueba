package com.example.cineprueba

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

data class MoviesResponse(
    @SerializedName("movies") private val _movies: List<MovieApiObject>?,
    @SerializedName("routes") private val _routes: List<RouteApiObject>?):Parcelable {

    val movies: List<MovieApiObject>
        get() = this._movies ?: emptyList()

    val routes: List<RouteApiObject>
        get() = this._routes ?: emptyList()

    constructor(parcel: Parcel) : this(
        parcel.createTypedArrayList(MovieApiObject) ?: emptyList(),
        parcel.createTypedArrayList(RouteApiObject) ?: emptyList()
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeTypedList(_movies)
        parcel.writeTypedList(_routes)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<MoviesResponse> {
        override fun createFromParcel(parcel: Parcel): MoviesResponse {
            return MoviesResponse(parcel)
        }

        override fun newArray(size: Int): Array<MoviesResponse?> {
            return arrayOfNulls(size)
        }
    }
}