package com.example.cineprueba

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

data class SizeApiObject(@SerializedName("large") private val _large: String?,
                         @SerializedName("medium") private val _medium: String?,
                         @SerializedName("small") private val _small: String?):Parcelable {
    val large: String
        get() = this._large ?: ""

    val medium: String
        get() = this._medium ?: ""

    val small: String
        get() = this._small ?: ""

    constructor(parcel: Parcel) : this(
        parcel.readString() ?: "",
        parcel.readString() ?: "",
        parcel.readString() ?: ""
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(_large)
        parcel.writeString(_medium)
        parcel.writeString(_small)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<SizeApiObject> {
        override fun createFromParcel(parcel: Parcel): SizeApiObject {
            return SizeApiObject(parcel)
        }

        override fun newArray(size: Int): Array<SizeApiObject?> {
            return arrayOfNulls(size)
        }
    }
}
