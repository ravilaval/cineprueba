package com.example.cineprueba

import retrofit2.Response
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.Header
import retrofit2.http.POST

interface TokenService {
    @FormUrlEncoded
    @POST("v2/oauth/token")
    suspend fun getUserToken(@Header("api_key") apiKey: String,
                     @Field("country_code") countryCode: String,
                     @Field("username") userName: String,
                     @Field("password") password: String,
                     @Field("grant_type") grantType: String,
                     @Field("client_id") clientId: String,
                     @Field("client_secret") clientSecret: String): Response<TokenResponse>
}