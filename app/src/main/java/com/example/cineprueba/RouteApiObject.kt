package com.example.cineprueba

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

data class RouteApiObject(@SerializedName("code") private val _code: String?,
                          @SerializedName("sizes") val sizes: SizeApiObject?):Parcelable {
    val code: String
        get() = this._code ?: ""

    constructor(parcel: Parcel) : this(
        parcel.readString() ?: "",
        parcel.readParcelable(SizeApiObject::class.java.classLoader)
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(_code)
        parcel.writeParcelable(sizes, flags)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<RouteApiObject> {
        override fun createFromParcel(parcel: Parcel): RouteApiObject {
            return RouteApiObject(parcel)
        }

        override fun newArray(size: Int): Array<RouteApiObject?> {
            return arrayOfNulls(size)
        }
    }
}
